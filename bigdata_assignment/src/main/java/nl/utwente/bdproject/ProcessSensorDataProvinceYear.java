package nl.utwente.bdproject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

/**
 * Aggregates and calculates the average traffic intensity,
 * grouped on province per year.
 * <p>
 * Only implemented for Provinces Utrecht, Zuid-Holland and Noord-Brabant.
 * <p>
 * Usage: 
 * <p>
 * hadoop jar [path_to_jar_file] nl.utwente.bdproject.ProcessSensorDataProvinceYear [sensor_metadata_file] [sensor_data_file] [output_dir]
 * 
 * @author Managing Big Data Group 1
 *
 */

public class ProcessSensorDataProvinceYear {
	
	public static class ExampleMapper extends Mapper<Object, Text, Text, Text> {
		
		private static Map<String,Province> sensors;
		
		@Override
		protected void setup(Context context) throws IOException,
				InterruptedException {
			super.setup(context);
			Configuration conf = context.getConfiguration();
			FileSystem fs = FileSystem.get(conf);
			BufferedReader bf = new BufferedReader(new InputStreamReader(
					fs.open(new Path(context.getCacheFiles()[0].getPath()))));
			
			String setupData = null;
			sensors = new HashMap<>();
			// Skip header line
			bf.readLine();
			while ((setupData = bf.readLine()) != null) {
				String[] tokens = setupData.split(",");
				double latitude = Double.parseDouble(tokens[1]);
				double longitude = Double.parseDouble(tokens[2]);
				int roadnumber = Integer.parseInt(tokens[5].substring(1));
				sensors.put(tokens[0],
						findProvince(roadnumber, latitude, longitude));
			}
			
		}
		
		@Override
		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {

			String[] item = value.toString().split(",");
			String refName = item[0];
			int year = Integer.parseInt(item[3]);
			int day = Integer.parseInt(item[4]);
			double counted = Double.parseDouble(item[5]);
			Province province = sensors.get(refName);
			if (province != null)
				context.write(new Text(province + "_" + year), new Text("" + counted));
		}
	}

	public static class ExampleReducer extends Reducer<Text, Text, Text, Text> {

		public void reduce(Text key, Iterable<Text> values, Context context)
				throws IOException, InterruptedException {
			double sum = 0;
			int cnt = 0;
			for (Text value : values) {
				sum += Double.parseDouble(value.toString());
				cnt++;
			}
			context.write(key, new Text("" + ((double) sum / cnt)));
		}
	}
	
	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
	
		String[] otherArgs = new GenericOptionsParser(conf, args)
				.getRemainingArgs();
		if (otherArgs.length < 3) {
			System.err.println("Usage: ProcessSensorDataProvinceYear <cachefile> <in> [<in>...] <out>");
			System.exit(2);
		}
		Job job = new Job(conf, "ProcessSensorDataProvinceYear");
		job.setJarByClass(ProcessSensorData.class);
		job.setMapperClass(ExampleMapper.class);
		job.setReducerClass(ExampleReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		job.addCacheFile(new Path(otherArgs[0]).toUri());
		for (int i = 1; i < otherArgs.length - 1; ++i) {
			FileInputFormat.addInputPath(job, new Path(otherArgs[i]));
		}
		FileOutputFormat.setOutputPath(job, new Path(
				otherArgs[otherArgs.length - 1]));
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}

	private enum Province { Utrecht, ZuidHolland, NoordBrabant };
	
	/**
	 * Implements a mapping from road and GPS coords to province.
	 * <p>
	 * Only implemented for Utrecht, Zuid-Holland and Noord-Brabant
	 * 
	 * @param roadname
	 * @param latitude
	 * @param longitude
	 * @return
	 */
	private static Province findProvince(int roadname, double latitude, double longitude)
	{
		/*
		RULES:

		Utrecht:
		A1 Longitude between 5.230852, 5.456036
		A2 Latitude between 51.937690, 52.278266
		A12 Longitude between 4.852144, 5.590023
		A27 Latitude between 51.955582, 52.178178
		A28 Latitude less than 52.204730
		
		Zuid-Holland:
		A3 Latitude less than 51.822668
		A4 Latitude between 51.873222, 52.215372
		A12 Longitude less than 4.852144
		A13
		A15 Longitude less than 5.007041
		A16 Latitude more than 51.716745
		A20 
		A27 Longitude between 4.942769, 5.055075
		A29
		A44 Latitude less than 52.224102
		N15
		N44
		
		Noord-Brabant:
		A2 Latitude less than 51.738795
		A4 Latitude less than 51.695341
		A16 Latitude less than 51.716745
		A17
		A27 Latitude less than 51.827533
		A50 Latitude less than 51.794447
		A65
		A73 Latitude between 51.758607, 51.567117
		N65
		 */
		
		switch (roadname) {
		case 1:
			if (longitude >= 5.230852 && longitude <= 5.456036)
				return Province.Utrecht;
			return null;
		case 2:
			if (latitude >= 51.937690 && latitude <= 52.278266)
				return Province.Utrecht;
			if (latitude <= 51.738795)
				return Province.NoordBrabant;
			return null;
		case 3:
			if (latitude <= 51.822668)
				return Province.ZuidHolland;
			return null;
		case 4:
			if (latitude >= 51.873222 && latitude <= 52.215372)
				return Province.ZuidHolland;
			if (latitude <= 51.695341)
				return Province.NoordBrabant;
			return null;
		case 12:
			if (longitude >= 4.852144 && longitude <= 5.590023)
				return Province.Utrecht;
			if (longitude <= 4.852144)
				return Province.ZuidHolland;
			return null;
		case 13:
			return Province.ZuidHolland;
		case 15:
			if (longitude <= 5.007041)
				return Province.ZuidHolland;
			return null;
		case 16:
			if (latitude >= 51.716745)
				return Province.ZuidHolland;
			return Province.NoordBrabant;
		case 17:
			return Province.NoordBrabant;
		case 20:
			return Province.ZuidHolland;
		case 27:
			if (latitude >= 52.178178 && latitude <= 51.955582)
				return Province.Utrecht;
			if (longitude >= 4.942769 && longitude <= 5.055075)
				return Province.ZuidHolland;
			if (latitude <= 51.827533)
				return Province.NoordBrabant;
			return null;
		case 28:
			if (latitude <= 52.204730)
				return Province.Utrecht;
			return null;
		case 29:
			return Province.ZuidHolland;
		case 44:
			if (latitude <= 52.224102)
				return Province.ZuidHolland;
			return null;
		case 50:
			if (latitude <= 51.794447)
				return Province.NoordBrabant;
			return null;
		case 65:
			return Province.NoordBrabant;
		case 73:
			if (latitude >= 51.567117 && latitude <= 51.758607)
				return Province.NoordBrabant;
			return null;
		}
		return null;
	}
	
}
