package nl.utwente.bdproject;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

/**
 * Aggregates and calculates the average traffic intensity,
 * grouped on quartile.
 * <p>
 * Usage: 
 * <p>
 * hadoop jar [path_to_jar_file] nl.utwente.bdproject.ProcessSensorDataProvinceYear [sensor_data_file] [output_dir]
 * 
 * @author Managing Big Data Group 1
 *
 */

public class ProcessSensorData {

	private static final int QUARTILE_1_END = 31 + 28 + 31;
	private static final int QUARTILE_2_END = QUARTILE_1_END + 30 + 31 + 30;
	private static final int QUARTILE_3_END = QUARTILE_2_END + 31 + 30 + 31;
	

	public static class ExampleMapper extends Mapper<Object, Text, Text, Text> {

		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {

			String[] item = value.toString().split(",");
			String refName = item[0];
			int year = Integer.parseInt(item[3]);
			int day = Integer.parseInt(item[4]);
			double counted = Double.parseDouble(item[5]);
			context.write(new Text("" + year + "_" + dayToQuartile(day)), new Text("" + counted));
		}
	}

	public static class ExampleReducer extends Reducer<Text, Text, Text, Text> {

		public void reduce(Text key, Iterable<Text> values, Context context)
				throws IOException, InterruptedException {
			double sum = 0;
			int cnt = 0;
			for (Text value : values) {
				sum += Double.parseDouble(value.toString());
				cnt++;
			}
			context.write(key, new Text("" + ((double) sum / cnt)));
		}
	}
	
	/**
	 * Mapping of a day number to a quartile. Does not take leap years into account.
	 * However, the impact of leap years will be negligible.
	 * 
	 * @param day
	 * @return
	 */
	private static int dayToQuartile(int day)
	{
		if (day <= QUARTILE_1_END)
			return 1;
		if (day <= QUARTILE_2_END)
			return 2;
		if (day <= QUARTILE_3_END)
			return 3;
		return 4;
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		String[] otherArgs = new GenericOptionsParser(conf, args)
				.getRemainingArgs();
		if (otherArgs.length < 2) {
			System.err.println("Usage: ProcessSensorData <in> [<in>...] <out>");
			System.exit(2);
		}
		Job job = new Job(conf, "ProcessSensorData");
		job.setJarByClass(ProcessSensorData.class);
		job.setMapperClass(ExampleMapper.class);
		job.setReducerClass(ExampleReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		for (int i = 0; i < otherArgs.length - 1; ++i) {
			FileInputFormat.addInputPath(job, new Path(otherArgs[i]));
		}
		FileOutputFormat.setOutputPath(job, new Path(
				otherArgs[otherArgs.length - 1]));
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
	

	

}
