# README #

This repository contains two scripts for aggregating sensor data with MapReduce on a hadoop cluster.

The first script, nl.utwente.bdproject.ProcessSensorData, groups the sensor data on quartile and calculates the average number of cars that pass each sensor per day.

The second script, nl.utwente.bdproject.ProcessSensorDataProvinceYear, groups the sensor data on year and Dutch province, and also calculates the average number of cars that each sensor per day.

The logic for selecting the province is deducted manually and only implemented for the provinces Utrecht, Zuid-Holland and Noord-Brabant.

### How do I get set up? ###

This project can be imported in Eclipse as an existing project.

It needs to be run on a hadoop cluster, and two files are needed: RSD.cvs.gz and sensors.csv. If you have sufficient access on the designated Managing Big Data machine, these files can be found on the hadoop filesystem in this location: /user/s1484885

In the project's root folder, use 'mvn package' to build the project.

Copy the resulting jar file, bigdata-0.2.jar to the hadoop cluster.

Then, assuming that the jar is in the current directory, and that the files RSD.csv.gz and sensors.csv are in the home folder of the user on the hadoop filesystem, use the following 2 commands to run the two respective scripts:


```
#!linux

hadoop jar bigdata-0.2.jar nl.utwente.bdproject.ProcessSensorData RSD.csv.gz result
hadoop jar bigdata-0.2.jar nl.utwente.bdproject.ProcessSensorDataProvinceYear sensors.csv RSD.csv.gz result2
```


The results will be written to respectively the result and result2 folder on the hadoop file system, so to view them we can do:


```
#!unix

hadoop fs -cat result/*
hadoop fs -cat result2/*
```


*** Authors ***

Managing Big Data Group 1

Wim van der Zijden <w.vanderzijden@student.utwente.nl>

Tristan Brugman <t.w.r.m.brugman@student.utwente.nl>